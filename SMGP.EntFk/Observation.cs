//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMGP.EntFk
{
    using System;
    using System.Collections.Generic;
    
    public partial class Observation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Observation()
        {
            this.Documents = new HashSet<Document>();
        }
    
        public int ObservationID { get; set; }
        public Nullable<int> StateID { get; set; }
        public Nullable<int> UserID { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> ModificationDate { get; set; }
        public Nullable<int> JustificationID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Document> Documents { get; set; }
        public virtual Parameter Parameter { get; set; }
        public virtual Parameter Parameter1 { get; set; }
        public virtual User User { get; set; }
    }
}
