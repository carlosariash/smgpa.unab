//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMGP.EntFk
{
    using System;
    using System.Collections.Generic;
    
    public partial class Permission
    {
        public System.Guid idPermission { get; set; }
        public string TextLink { get; set; }
        public string Controller { get; set; }
        public string ActionResult { get; set; }
        public bool ActiveMenu { get; set; }
    }
}
