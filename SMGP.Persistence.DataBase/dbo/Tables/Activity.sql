﻿CREATE TABLE [dbo].[Activity] (
    [ActivityID]    INT            IDENTITY (1, 1) NOT NULL,
    [Name]          NVARCHAR (MAX) NOT NULL,
    [Description]   NVARCHAR (MAX) NULL,
    [StateID]       INT            NOT NULL,
    [StartDate]     DATETIME2 (7)  NULL,
    [EndDate]       DATETIME2 (7)  NULL,
    [ResponsableID] INT            NULL,
    [CarrerID]      INT            NULL,
    [FacultyID]     INT            NULL,
    [UniversityID]  INT            NULL,
    CONSTRAINT [PK_Activity] PRIMARY KEY CLUSTERED ([ActivityID] ASC),
    CONSTRAINT [FK_Activity_Career] FOREIGN KEY ([CarrerID]) REFERENCES [dbo].[Career] ([CareerID]),
    CONSTRAINT [FK_Activity_Faculty] FOREIGN KEY ([FacultyID]) REFERENCES [dbo].[Faculty] ([FacultyID]),
    CONSTRAINT [FK_Activity_Parameter] FOREIGN KEY ([StateID]) REFERENCES [dbo].[Parameter] ([ParameterID]),
    CONSTRAINT [FK_Activity_University] FOREIGN KEY ([UniversityID]) REFERENCES [dbo].[University] ([UniversityID]),
    CONSTRAINT [FK_Activity_User] FOREIGN KEY ([ResponsableID]) REFERENCES [dbo].[User] ([UserID])
);




GO



GO


