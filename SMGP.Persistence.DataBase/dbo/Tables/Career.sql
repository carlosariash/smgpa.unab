﻿CREATE TABLE [dbo].[Career] (
    [CareerID]         INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (MAX) NOT NULL,
    [Descripcion]      NVARCHAR (MAX) CONSTRAINT [DF__Career__Descripc__01142BA1] DEFAULT ('') NULL,
    [FacultyID]        INT            NULL,
    [ResponsibleID]    INT            NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    CONSTRAINT [PK_Career] PRIMARY KEY CLUSTERED ([CareerID] ASC),
    CONSTRAINT [FK_Career_Faculty] FOREIGN KEY ([FacultyID]) REFERENCES [dbo].[Faculty] ([FacultyID]),
    CONSTRAINT [FK_Career_User] FOREIGN KEY ([ResponsibleID]) REFERENCES [dbo].[User] ([UserID])
);



