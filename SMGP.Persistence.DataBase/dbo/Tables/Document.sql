﻿CREATE TABLE [dbo].[Document] (
    [DocumentID]       INT            IDENTITY (1, 1) NOT NULL,
    [Path]             NVARCHAR (MAX) NULL,
    [ActivityID]       INT            NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    [ObservationID]    INT            NULL,
    [TaskID]           INT            NULL,
    CONSTRAINT [PK_Document] PRIMARY KEY CLUSTERED ([DocumentID] ASC),
    CONSTRAINT [FK_Document_Activity] FOREIGN KEY ([ActivityID]) REFERENCES [dbo].[Activity] ([ActivityID]),
    CONSTRAINT [FK_Document_Observation] FOREIGN KEY ([ObservationID]) REFERENCES [dbo].[Observation] ([ObservationID]),
    CONSTRAINT [FK_Document_Tasks] FOREIGN KEY ([TaskID]) REFERENCES [dbo].[Tasks] ([TaskID])
);




GO


