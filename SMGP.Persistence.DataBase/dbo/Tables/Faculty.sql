﻿CREATE TABLE [dbo].[Faculty] (
    [FacultyID]        INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (MAX) NULL,
    [UniversityID]     INT            NOT NULL,
    [Description]      NVARCHAR (MAX) NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    CONSTRAINT [PK_Faculty] PRIMARY KEY CLUSTERED ([FacultyID] ASC),
    CONSTRAINT [FK_Faculty_University] FOREIGN KEY ([FacultyID]) REFERENCES [dbo].[University] ([UniversityID])
);



