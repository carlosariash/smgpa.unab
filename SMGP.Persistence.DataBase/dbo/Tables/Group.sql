﻿CREATE TABLE [dbo].[Group] (
    [GroupID]          INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (MAX) NULL,
    [Description]      NVARCHAR (MAX) NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED ([GroupID] ASC)
);

