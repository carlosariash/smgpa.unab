﻿CREATE TABLE [dbo].[Member] (
    [MemberID]         INT           IDENTITY (1, 1) NOT NULL,
    [PersonalID]       INT           NULL,
    [ActivityID]       INT           NULL,
    [ModificationDate] DATETIME2 (7) NULL,
    [TaskID]           INT           NULL,
    CONSTRAINT [PK_Member] PRIMARY KEY CLUSTERED ([MemberID] ASC),
    CONSTRAINT [FK_Member_Activity] FOREIGN KEY ([ActivityID]) REFERENCES [dbo].[Activity] ([ActivityID]),
    CONSTRAINT [FK_Member_Tasks] FOREIGN KEY ([TaskID]) REFERENCES [dbo].[Tasks] ([TaskID]),
    CONSTRAINT [FK_Member_User] FOREIGN KEY ([PersonalID]) REFERENCES [dbo].[User] ([UserID])
);

