﻿CREATE TABLE [dbo].[Notificacion] (
    [idNotification] UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [Cuerpo]         NVARCHAR (MAX)   NULL,
    [Fecha]          DATETIME         NULL,
    [Vista]          BIT              NOT NULL,
    [idUser]         UNIQUEIDENTIFIER NOT NULL,
    [UrlAction]      NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_dbo.Notificacion] PRIMARY KEY CLUSTERED ([idNotification] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_idUser]
    ON [dbo].[Notificacion]([idUser] ASC);

