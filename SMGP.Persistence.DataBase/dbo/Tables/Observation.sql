﻿CREATE TABLE [dbo].[Observation] (
    [ObservationID]    INT            IDENTITY (1, 1) NOT NULL,
    [StateID]          INT            NULL,
    [UserID]           INT            NULL,
    [Description]      NVARCHAR (200) NOT NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    [JustificationID]  INT            NULL,
    CONSTRAINT [PK_Observation] PRIMARY KEY CLUSTERED ([ObservationID] ASC),
    CONSTRAINT [FK_Observation_Justification] FOREIGN KEY ([JustificationID]) REFERENCES [dbo].[Parameter] ([ParameterID]),
    CONSTRAINT [FK_Observation_State] FOREIGN KEY ([StateID]) REFERENCES [dbo].[Parameter] ([ParameterID]),
    CONSTRAINT [FK_Observation_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])
);




GO



GO


