﻿CREATE TABLE [dbo].[Parameter] (
    [ParameterID]      INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (MAX) NULL,
    [Description]      NVARCHAR (MAX) NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    [TypeParameterID]  INT            NOT NULL,
    CONSTRAINT [PK_Parameter] PRIMARY KEY CLUSTERED ([ParameterID] ASC),
    CONSTRAINT [FK_Parameter_TypeParameter] FOREIGN KEY ([TypeParameterID]) REFERENCES [dbo].[TypeParameter] ([TypeParameterID])
);

