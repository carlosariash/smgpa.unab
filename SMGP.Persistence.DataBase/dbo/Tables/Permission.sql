﻿CREATE TABLE [dbo].[Permission] (
    [idPermission] UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [TextLink]     NVARCHAR (MAX)   NOT NULL,
    [Controller]   NVARCHAR (MAX)   NOT NULL,
    [ActionResult] NVARCHAR (MAX)   NOT NULL,
    [ActiveMenu]   BIT              NOT NULL,
    CONSTRAINT [PK_dbo.Permission] PRIMARY KEY CLUSTERED ([idPermission] ASC)
);

