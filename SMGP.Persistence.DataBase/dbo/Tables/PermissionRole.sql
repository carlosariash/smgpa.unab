﻿CREATE TABLE [dbo].[PermissionRole] (
    [RolePermissionID] INT           IDENTITY (1, 1) NOT NULL,
    [RoleID]           INT           NULL,
    [PermissionID]     INT           NULL,
    [ModificationDate] DATETIME2 (7) NULL,
    CONSTRAINT [PK_PermissionRole] PRIMARY KEY CLUSTERED ([RolePermissionID] ASC)
);




GO



GO


