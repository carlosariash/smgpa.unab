﻿CREATE TABLE [dbo].[Tasks] (
    [TaskID]           INT           IDENTITY (1, 1) NOT NULL,
    [StartDate]        DATETIME2 (7) NULL,
    [EndDate]          DATETIME2 (7) NULL,
    [StateID]          INT           NOT NULL,
    [ActivityID]       INT           NULL,
    [ModificationDate] DATETIME2 (7) NULL,
    [ResponsableID]    INT           NULL,
    CONSTRAINT [PK_Tasks] PRIMARY KEY CLUSTERED ([TaskID] ASC),
    CONSTRAINT [FK_Tasks_Activity] FOREIGN KEY ([ActivityID]) REFERENCES [dbo].[Activity] ([ActivityID]),
    CONSTRAINT [FK_Tasks_Parameter] FOREIGN KEY ([StateID]) REFERENCES [dbo].[Parameter] ([ParameterID]),
    CONSTRAINT [FK_Tasks_User] FOREIGN KEY ([ResponsableID]) REFERENCES [dbo].[User] ([UserID])
);




GO



GO



GO



GO



GO



GO


