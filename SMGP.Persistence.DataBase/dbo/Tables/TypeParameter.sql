﻿CREATE TABLE [dbo].[TypeParameter] (
    [TypeParameterID]  INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (MAX) NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    CONSTRAINT [PK_TypeParameter] PRIMARY KEY CLUSTERED ([TypeParameterID] ASC)
);

