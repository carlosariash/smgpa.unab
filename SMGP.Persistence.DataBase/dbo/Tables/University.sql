﻿CREATE TABLE [dbo].[University] (
    [UniversityID]     INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (MAX) NULL,
    [Description]      NVARCHAR (MAX) NULL,
    [ProfileImage]     NVARCHAR (MAX) NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    CONSTRAINT [PK_University_1] PRIMARY KEY CLUSTERED ([UniversityID] ASC)
);



