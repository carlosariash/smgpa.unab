﻿CREATE TABLE [dbo].[User] (
    [UserID]           INT            IDENTITY (1, 1) NOT NULL,
    [TypeUser]         INT            NULL,
    [Run]              NVARCHAR (MAX) NOT NULL,
    [Name]             NVARCHAR (MAX) NOT NULL,
    [LastName]         NVARCHAR (MAX) NOT NULL,
    [CorporateMail]    NVARCHAR (MAX) NOT NULL,
    [PersonalMail]     NVARCHAR (MAX) NULL,
    [Password]         NVARCHAR (MAX) NOT NULL,
    [Mobile]           NVARCHAR (MAX) NULL,
    [RoleID]           INT            NULL,
    [CareerID]         INT            NULL,
    [VenuesID]         INT            NULL,
    [GroupID]          INT            NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([UserID] ASC),
    CONSTRAINT [FK_User_Career] FOREIGN KEY ([CareerID]) REFERENCES [dbo].[Career] ([CareerID]),
    CONSTRAINT [FK_User_Group] FOREIGN KEY ([GroupID]) REFERENCES [dbo].[Group] ([GroupID]),
    CONSTRAINT [FK_User_Role] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Role] ([RoleID]),
    CONSTRAINT [FK_User_Venue] FOREIGN KEY ([VenuesID]) REFERENCES [dbo].[Venue] ([VenueID])
);



