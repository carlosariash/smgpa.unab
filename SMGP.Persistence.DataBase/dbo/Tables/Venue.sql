﻿CREATE TABLE [dbo].[Venue] (
    [VenueID]          INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (MAX) NULL,
    [Description]      NVARCHAR (MAX) NULL,
    [CareerID]         INT            NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    CONSTRAINT [PK_Venues] PRIMARY KEY CLUSTERED ([VenueID] ASC),
    CONSTRAINT [FK_Venues_Career] FOREIGN KEY ([CareerID]) REFERENCES [dbo].[Career] ([CareerID])
);

