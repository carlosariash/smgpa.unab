using SMGP.EntFk;
using SMGPA.Core.Interfaces;
using SMGPA.Core.Services;
using SMGPA.Domain.Interfaces;
using SMGPA.Domain.Seedwork;
using SMGPA.Domain.Services;
using SMGPA.Infrastructure.Adapting;
using SMGPA.Infrastructure.Domain;
using SMGPA.Repositories.Repositories;
using System;
using System.Data.Entity;
using Unity;
using Unity.Lifetime;

namespace SMGPA.Api
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {


            container.RegisterType<SMGPAEntities>();


            // Entity Framework
            container.RegisterType<DbContext, SMGPAEntities>();

            // Crosscutting
            container.RegisterType<IUnitOfWork, MainUnitOfWork>(new PerResolveLifetimeManager());
            container.RegisterType<ITypeAdapter, AutomapperTypeAdapter>(new HierarchicalLifetimeManager());


            // Core Services

            container.RegisterType<IVenueApplication, VenueApplication>();


            // Domain Services
            container.RegisterType<IVenuesService, VenuesService>();


            // Repository Services

            container.RegisterType<IVenuesRepository, VenuesRepository>();


            TypeAdapterFactory.SetCurrent(container.Resolve<ITypeAdapter>());
           // CacheProviderFactory.SetCurrent(container.Resolve<ICacheProvider>());




        }
    }
}