﻿using SMGPA.Core.Interfaces;
using SMGPA.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace SMGPA.Api.Controllers
{
    public class VenuesController : ApiController
    {
        IVenueApplication _IVenueApplication;
        public VenuesController(IVenueApplication IVenueApplication)
        {
            _IVenueApplication = IVenueApplication;
        }



        [HttpPost]
        public HttpStatusCode InsertOrUpdate([FromBody] Venues item )
        {
            _IVenueApplication.InsertOrUpdate(item);
            return HttpStatusCode.OK;

        }

    }
}