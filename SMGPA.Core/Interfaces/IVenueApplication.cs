﻿using SMGPA.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Core.Interfaces
{
    public interface IVenueApplication 
    {
        void InsertOrUpdate(Venues item);
    }
}
