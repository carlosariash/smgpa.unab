﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Core.Security
{
    /// <summary>
    /// Interfaz de proveedor de servicios de membresía.
    /// </summary>
    /// <typeparam name="T">Tipo de dato de la clave primaria del usuario.</typeparam>
    /// <typeparam name="K">Tipo de objeto que contiene la información de logueo.</typeparam>
    public interface IMembershipProvider : IDisposable
    {
        /// <summary>
        /// Devuelve el usuario Actual.
        /// </summary>
        /// <typeparam name="T">Tipo de dato de la clave primaria del usuario.</typeparam>
        /// <returns>Usuario logueado.</returns>
        UserInfo<T> GetCurrentUser<T>();

        /// <summary>
        /// Valida las credenciales del usuario y realiza el ingreso al sistema.
        /// </summary>
        /// <param name="loginInfo">Objeto que contiene la información login.</param>
        /// <typeparam name="T">Tipo de dato de la clave primaria del usuario.</typeparam>
        /// <typeparam name="K">Tipo de objeto que contiene la información de logueo.</typeparam>
        /// <returns>Usuario autenticado o NULL.</returns>
        UserInfo<T> Login<T, K>(K loginInfo);

        /// <summary>
        /// Realiza la salida del sistema.
        /// </summary>
        /// <returns>TRUE si se pudo realizar la operació.</returns>
        bool Logout();
    }
}
