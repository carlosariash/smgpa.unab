﻿using SMGPA.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Core.Security
{
    public class MembershipProvider : IMembershipProvider
    {

        public const string CACHE_USER_KEY_PREFIX = "UserName_";

        public const string HEADER_USER_IDENTITY_NAME = "UserName";

        public const string HEADER_TOKEN_NAME = "Authorization";

        public const string HEADER_USER_HOSTNAME = "ClientUserHostName";



        private IUsersService _IUsersService;

        public MembershipProvider(IUsersService IUsersService)
        {

            _IUsersService = IUsersService;

        }


        public void Dispose()
        {
            _IUsersService.Dispose();
        }

        public UserInfo<T> GetCurrentUser<T>()
        {
            throw new NotImplementedException();
        }

        public UserInfo<T> Login<T, K>(K loginInfo)
        {
            throw new NotImplementedException();
        }

        public bool Logout()
        {
            throw new NotImplementedException();
        }
    }
}
