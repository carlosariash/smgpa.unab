﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Core.Security
{
    public enum LoginStatus
    {
        NoLogged = 0,
        Logged = 1,
        Locked = 2,
        Expired = 3,
    }

    public class UserInfo<T>
    {

        /// <summary>
        /// Obtiene o establece el identificador del usuario.
        /// </summary>
        public T UserId { get; set; }

        /// <summary>
        /// Obtiene o establece el nombre de la cuenta del usuario.
        /// </summary>
        public string UserName { get; set; }


        /// <summary>
        /// Obtiene o establece el nombre del usuario.
        /// </summary
        /// 

        public string Name { get; set; }



        public string LastName { get; set; }


        /// <summary>
        /// Obtiene o establece el token del usuario.
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Obtiene o establece la cantidad de intentos de logueo.
        /// </summary>
        public int AttemptsCount { get; set; }

        /// <summary>
        /// Obtiene o establece la lista de permisos del usuario.
        /// </summary>
      //  public List<Domain.Seedwork.Entities.ModuleViewPermission> Permissions { get; set; }

        /// <summary>
        /// Obtiene o establece la culttura del usuario.
        /// </summary>
        public CultureInfo CurrentCulture { get; set; }

        /// <summary>
        /// Obtiene o establece la última fecha de logueo.
        /// </summary>
        public DateTime LastLoginDate { get; set; }

        /// <summary>
        /// Obtiene o establece el estado de logueo del usuario.
        /// </summary>
        public LoginStatus Status { get; set; }



    }
}
