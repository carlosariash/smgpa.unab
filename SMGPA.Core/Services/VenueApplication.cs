﻿using SMGPA.Core.Interfaces;
using SMGPA.Domain.Entities;
using SMGPA.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Core.Services
{
    public class VenueApplication : IVenueApplication
    {
        IVenuesService _IVenuesService;
        public VenueApplication(IVenuesService IVenuesService)
        {
            _IVenuesService = IVenuesService;
        }
        public void InsertOrUpdate(Venues item)
        {
            _IVenuesService.Add(item);
        }
    }
}
