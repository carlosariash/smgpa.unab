﻿using SMGPA.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Domain
{
    public abstract class DomainService<TEntity> : IDomainService<TEntity>
      where TEntity : Entity
    {
        public abstract TEntity Add(TEntity pItem);
        public abstract bool Delete(object pID);
        public abstract IEnumerable<TEntity> GetAll();
        public abstract TEntity GetById(object pID);
        public abstract TEntity Update(TEntity pItem);
        public abstract List<TEntity> InsertOrUpdateList(List<TEntity> pItem);
        public abstract void Dispose();

    }
}
