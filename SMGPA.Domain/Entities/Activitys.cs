﻿using SMGPA.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Domain.Entities
{
    public class Activitys : Entity
    {
        [EntityKey]
        public int ActivityID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int StateID { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int ResponsableID { get; set; }

        public int? CarrerID { get; set; }

        public int? FacultyID { get; set; }

        public int? UniversityID { get; set; }

        public DateTime ModificationDate { get; set; }
    }
}
