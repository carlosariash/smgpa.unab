﻿using SMGPA.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Domain.Entities
{
    public class Careers : Entity
    {
        [EntityKey]
        public int CareerID { get; set; }

        public string Name { get; set; }

        public string Descripcion { get; set; }

        public int FacultyID { get; set; }

        public int ResponsibleID { get; set; }

        public DateTime ModificationDate { get; set; }
    }
}
