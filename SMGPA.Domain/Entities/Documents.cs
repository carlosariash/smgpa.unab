﻿using SMGPA.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Domain.Entities
{
    public class Documents : Entity
    {
        [EntityKey]
        public int DocumentID { get; set; }

        public string Path { get; set; }

        public int? ActivityID { get; set; }

        public int? ObservationID { get; set; }

        public int? TaskID { get; set; }

        public DateTime ModificationDate { get; set; }
    }
}
