﻿using SMGPA.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Domain.Entities
{
    public class Members : Entity
    {
        [EntityKey]
        public int MemberID { get; set; }

        public int? PersonalID { get; set; }

        public int? ActivityID { get; set; }

        public DateTime ModificationDate { get; set; }

        public int? TaskID { get; set; }
    }
}
