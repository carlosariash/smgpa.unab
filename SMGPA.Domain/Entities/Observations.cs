﻿using SMGPA.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Domain.Entities
{
    public class Observations : Entity
    {
        [EntityKey]
        public int ObservationID { get; set; }

        public int? StateID { get; set; }

        public int? UserID { get; set; }

        public string Description { get; set; }

        public DateTime ModificationDate { get; set; }

        public int? JustificationID { get; set; }
    }
}
