﻿using SMGPA.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Domain.Entities
{
    public class Parameters : Entity
    {
        [EntityKey]
        public int ParameterID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime ModificationDate { get; set; }

        public int? TypeParameterID { get; set; }
    }
}
