﻿using SMGPA.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Domain.Entities
{
    public class PermissionRoles : Entity
    {
        [EntityKey]
        public int RolePermissionID { get; set; }

        public int? RoleID { get; set; }

        public int? PermissionID { get; set; }

        public DateTime ModificationDate { get; set; }
    }
}
