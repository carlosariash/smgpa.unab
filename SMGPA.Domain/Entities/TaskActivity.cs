﻿using SMGPA.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Domain.Entities
{
    public class TaskActivity : Entity
    {
        [EntityKey]
        public int TaskID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? StateID { get; set; }
        public int? ActivityID { get; set; }
        public int? ResponsableID { get; set; }
        public DateTime ModificationDate { get; set; }
    }
}
