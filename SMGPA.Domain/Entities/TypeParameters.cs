﻿using SMGPA.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Domain.Entities
{
    public class TypeParameters:  Entity
    {
        [EntityKey]
        public int TypeParameterID { get; set; }

        public string Name { get; set; }

        public DateTime ModificationDate { get; set; }
    }
}
