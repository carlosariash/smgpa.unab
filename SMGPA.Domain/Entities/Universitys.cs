﻿using SMGPA.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Domain.Entities
{
    public class Universitys : Entity
    {

        [EntityKey]
        public int UniversityID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ProfileImage { get; set; }

        public DateTime ModificationDate { get; set; }
    }
}
