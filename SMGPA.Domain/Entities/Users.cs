﻿using SMGPA.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Domain.Entities
{
    public class Users : Entity
    {
        [EntityKey]
        public int UserID { get; set; }

        public int? TypeUser { get; set; }

        public string Run { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string CorporateMail { get; set; }

        public string PersonalMail { get; set; }

        public string Password { get; set; }
        public string Mobile { get; set; }
        public int? RoleID { get; set; }
        public int? CareerID { get; set; }
        public int? VenuesID { get; set; }
        public int? GroupID { get; set; }

        public int? FacultyID { get; set; }
        public DateTime ModificationDate { get; set; }
    }
}
