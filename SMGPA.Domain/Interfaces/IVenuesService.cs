﻿using SMGPA.Domain.Entities;
using SMGPA.Domain.Seedwork;
using SMGPA.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Domain.Interfaces
{
    public interface IVenuesService : IDomainService<Venues>
    {
    }
}
