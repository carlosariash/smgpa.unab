﻿using SMGPA.Domain.Entities;
using SMGPA.Domain.Interfaces;
using SMGPA.Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Domain.Services
{
    public class VenuesService : DomainService<Venues>, IVenuesService
    {
        IVenuesRepository _IVenuesRepository;

        public VenuesService(IVenuesRepository IVenuesRepository)
        {

            _IVenuesRepository = IVenuesRepository;

        }

        public override Venues Add(Venues pItem)
        {
            _IVenuesRepository.Add(pItem);
            return pItem;
        }

        public override bool Delete(object pID)
        {
            _IVenuesRepository.Remove(pID);
            return true;
        }

        public override void Dispose()
        {
            _IVenuesRepository = null;
        }

        public override IEnumerable<Venues> GetAll()
        {
          return  _IVenuesRepository.GetAll();
        }

        public override Venues GetById(object pID)
        {
           return _IVenuesRepository.Get(pID);
        }

        public override List<Venues> InsertOrUpdateList(List<Venues> pItem)
        {
            throw new NotImplementedException();
        }

        public override Venues Update(Venues pItem)
        {
            _IVenuesRepository.Modify(pItem);
            return pItem;
        }
    }
}
