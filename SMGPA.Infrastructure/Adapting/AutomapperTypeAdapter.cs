﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Infrastructure.Adapting
{
    /// <summary>
    /// Adaptador utilizando Automapper.
    /// </summary>
    public class AutomapperTypeAdapter : ITypeAdapter
    {

        public AutomapperTypeAdapter()
        {
            /*
            var wProfiles = GetProfiles();

            // Busca los objetos de tipo Profile en todos los ensamblados.
            Mapper.Initialize(cfg =>
            {
              foreach (var pItem in wProfiles)
              {
                cfg.AddProfile(Activator.CreateInstance(pItem) as Profile);
              }
            });
            */
        }

        /// <summary>
        /// Convierte un tipo <paramref name="TSource"/> a <paramref name="TTarget"/>.
        /// </summary>
        /// <typeparam name="TSource">Tipo de objeto que se desea convertir.</typeparam>
        /// <typeparam name="TTarget">Tipo de objeto al cual se desea convertir.</typeparam>
        /// <param name="pSource">Objeto orígen.</param>
        /// <returns>Objeto convertido.</returns>
        public TTarget Adapt<TSource, TTarget>(TSource pSource)
          where TSource : class
          where TTarget : class, new()
        {
            return Mapper.Map<TSource, TTarget>(pSource);
        }

        /// <summary>
        /// Convierte a un tipo <paramref name="TTarget"/>.
        /// </summary>
        /// <typeparam name="TTarget">Tipo de objeto al cual se desea convertir.</typeparam>
        /// <param name="pSource">Objeto orígen.</param>
        /// <returns>Objeto convertido.</returns>
        public TTarget Adapt<TTarget>(object pSource)
          where TTarget : class, new()
        {
            return Mapper.Map<TTarget>(pSource);
        }

        /// <summary>
        /// Devuelve todos los objetos de tipo Profile (que utilizan Automapper) cargados en los ensamblados en memoria.
        /// </summary>
        /// <returns>Lista de objetos Profile.</returns>
        private IEnumerable<Type> GetProfiles()
        {
            return AppDomain.CurrentDomain.GetAssemblies()
              .SelectMany(a => a.GetTypes())
              .Where(type => !type.IsAbstract &&
                typeof(Profile).IsAssignableFrom(type) &&
                type.FullName != "AutoMapper.Profile");
        }

    }
}
