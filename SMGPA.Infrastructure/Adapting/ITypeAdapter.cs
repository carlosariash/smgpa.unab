﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Infrastructure.Adapting
{
    /// <summary>
    /// Interfaz que deben cumplir los adaptadores.
    /// </summary>
    public interface ITypeAdapter
    {
        TTarget Adapt<TSource, TTarget>(TSource pSource)
          where TTarget : class, new()
          where TSource : class;

        TTarget Adapt<TTarget>(object pSource)
          where TTarget : class, new();
    }
}
