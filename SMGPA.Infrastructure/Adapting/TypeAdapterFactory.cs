﻿using SMGPA.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Infrastructure.Adapting
{
    /// <summary>
    /// Factoría de adaptadores.
    /// </summary>
    public static class TypeAdapterFactory
    {

        #region Members

        static ITypeAdapter m_current = null;

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Asigna el adaptador actual.
        /// </summary>
        /// <param name="pCurrent">Adptador.</param>
        public static void SetCurrent(ITypeAdapter pCurrent)
        {
            m_current = pCurrent;
        }

        /// <summary>
        /// Devuelve el adaptador actual.
        /// </summary>
        public static ITypeAdapter Current
        {
            get { return m_current; }
        }

        /// <summary>
        /// Método extendido para entidades de dominio.
        /// Adapta un tipo de entidad de dominio a otro.
        /// </summary>
        /// <typeparam name="T">Tipo al que se desea adaptar.</typeparam>
        /// <param name="pItem">Entidad de dominio.</param>
        /// <returns>Tipo adaptado.</returns>
        public static T ConvertTo<T>(this IEntity pItem)
          where T : class, new()
        {
            return m_current.Adapt<T>(pItem);
        }

        /// <summary>
        /// Método extendido para objetos.
        /// Adapta un tipo a otro.
        /// </summary>
        /// <typeparam name="T">Tipo al que se desea adaptar.</typeparam>
        /// <param name="pItem">Entidad de dominio.</param>
        /// <returns>Tipo adaptado.</returns>
        public static T ConvertTo<T>(this object pItem)
          where T : class, new()
        {
            return m_current.Adapt<T>(pItem);
        }

        #endregion

    }
}
