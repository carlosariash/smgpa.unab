﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Infrastructure.Domain
{

    public abstract class Entity : IEntity, ICloneable
    {
        public virtual object GetEntityKey()
        {
            foreach (PropertyInfo wPInfo in this.GetType().GetProperties())
            {
                if (wPInfo.CustomAttributes.Any(p => p.AttributeType == typeof(EntityKeyAttribute)))
                {
                    return wPInfo.GetValue(this);
                }
            }
            throw new NullReferenceException(string.Format("La entidad de dominio '{0}' no posee declarada la clave principal mediante el atributo EntityKeyAttribute.", this.GetType().Name));
        }

        /// <summary>
        /// Asigna el valor de la clave principal de la entidad de dominio (a partir del atributo EntityKeyAttribute).
        /// </summary>
        /// <param name="pItem">Entidad de dominio.</param>
        /// <param name="value">Valor que se le desea asignar.</param>
        /// <remarks>
        /// Esto no funciona para entidades que poseen clave compuesta.
        /// </remarks>
        public virtual void SetEntityKey(object value)
        {
            foreach (PropertyInfo wPInfo in this.GetType().GetProperties())
            {
                if (wPInfo.CustomAttributes.Any(p => p.AttributeType == typeof(EntityKeyAttribute)))
                {
                    wPInfo.SetValue(this, value);
                }
            }
        }

        /// <summary>
        /// Devuelve una copia de la entidad de dominio.
        /// </summary>
        /// <returns>Entidad clonada.</returns>
        public object Clone()
        {
            return (Entity)this.MemberwiseClone();
        }

        /// <summary>
        /// Devuelve una copia tipificada de la entidad de dominio.
        /// </summary>
        /// <returns>Entidad clonada.</returns>
        public T Clone<T>() where T : Entity
        {
            return (T)this.MemberwiseClone();
        }

    }
}
