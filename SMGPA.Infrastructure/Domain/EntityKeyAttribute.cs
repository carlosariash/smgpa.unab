﻿using System;

namespace SMGPA.Infrastructure.Domain
{
    /// <summary>
    /// Atributo que se utiliza para especificar la clave principal de un objeto de dominio.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class EntityKeyAttribute : Attribute
    {

    }
}