﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Infrastructure.Domain
{
    public interface IDomainService<TEntity> : IDisposable
          where TEntity : Entity
    {
        /// <summary>
        /// Elimina un item.
        /// </summary>
        /// <param name="pID">Clave de identificación del item.</param>
        /// <returns>TRUE si se pudo realizar la eliminación.</returns>
        bool Delete(object pID);

        /// <summary>
        /// Devuelve todos las entidades.
        /// </summary>
        /// <returns>Entidades de dominio.</returns>
        IEnumerable<TEntity> GetAll();

        /// <summary>
        /// Devuelve un item a partir de su ID.
        /// </summary>
        /// <param name="pID">Clave de identificación del item.</param>
        /// <returns>Item recuperado.</returns>
        TEntity GetById(object pID);

        /// <summary>
        /// Agrega un nuevo item.
        /// </summary>
        /// <param name="pItem">Entidad de dominio.</param>
        /// <returns>Entidad de dominio actualizada.</returns>
        TEntity Add(TEntity pItem);

        /// <summary>
        /// Modifica un item.
        /// </summary>
        /// <param name="pItem">Entidad de dominio.</param>
        /// <returns>Entidad de dominio actualizada.</returns>
        TEntity Update(TEntity pItem);



        /// <summary>
        /// Inserta o Actualiza una lista.
        /// </summary>
        /// <param name="pItem"> Lista Entidad de dominio.</param>
        /// <returns> Listado Entidad de dominio actualizada.</returns>
        List<TEntity> InsertOrUpdateList(List<TEntity> pItem);

    }
}
