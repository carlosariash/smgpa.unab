﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Infrastructure.Domain
{
    /// <summary>
    /// Interfaz de entidades de dominio.
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Devuelve el valor de la clave principal de la entidad de dominio (a partir del atributo EntityKeyAttribute).
        /// </summary>
        /// <param name="pItem">Entidad de dominio.</param>
        /// <returns>Valor de la clave principal.</returns>
        /// <remarks>
        /// Esto no funciona para entidades que poseen clave compuesta.
        /// </remarks>
        object GetEntityKey();
    }
}
