﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Infrastructure.Domain
{
    /// <summary>
    /// Interfaz de repositorios para utilizar Entity Framework.
    /// </summary>
    /// <typeparam name="TEntity">Tipo de entidad de Entity Framework.</typeparam>
    /// <typeparam name="TEntityModel">Tipo de entidad de dominio.</typeparam>
    public interface IQueryableRepository<TEntity, TEntityModel> : IRepository<TEntityModel>
      where TEntityModel : Entity, new()
      where TEntity : class, new()
    {
        /// <summary>
        /// Convierte una entidade de contexto en una entidade de dominio.
        /// </summary>
        /// <param name="pEntity">Entidade de contexto.</param>
        /// <returns>Entidade de dominio.</returns>
        TEntityModel ConvertToModel(TEntity pEntity);

        /// <summary>
        /// Convierte una lista de entidades de contexto en una lista de entidades de dominio.
        /// </summary>
        /// <param name="pList">Lista de entidades de contexto.</param>
        /// <returns>Lista de entidades de dominio.</returns>
        IEnumerable<TEntityModel> ConvertToModelList(IEnumerable<TEntity> pList);




    }
}
