﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace SMGPA.Infrastructure.Domain
{
    /// <summary>
    /// Interfaz de unidad de trabajo para utilizar Entity Framework.
    /// </summary>
    public interface IQueryableUnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// Valida si la referencia de una entidad existe en el contexto.
        /// </summary>
        /// <typeparam name="TEntity">Tipo de entidad de contexto.</typeparam>
        /// <param name="PItem">Entidad de contexto.</param>
        /// <returns>TRUE si la entidad existe en el contexto.</returns>
        bool Exists<TEntity>(TEntity PItem) where TEntity : class;

        /// <summary>
        /// Devuelve una instancia de IDbSet para acceder al tipo definido en el contexto.
        /// </summary>
        /// <typeparam name="T">Tipo de entidad de contexto.</typeparam>
        /// <returns>Lista de entidades del contexto.</returns>
        DbSet<T> CreateSet<T>() where T : class;

        /// <summary>
        /// Adjunta la entidad  al contexto.
        /// </summary>
        /// <typeparam name="T">Tipo de entidad de contexto.</typeparam>
        /// <param name="pItem">Entidad de contexto.</param>
        void Attach<T>(T pItem) where T : class;

        /// <summary>
        /// Marca la entidad como modificada.
        /// </summary>
        /// <typeparam name="T">Tipo de entidad de contexto.</typeparam>
        /// <param name="pItem">Entidad de contexto.</param>
        void SetModified<TEntity>(TEntity pItem) where TEntity : class;

        /// <summary>
        /// Elimina la entidad de contexto.
        /// </summary>
        /// <typeparam name="T">Tipo de entidad de contexto.</typeparam>
        /// <param name="pItem">Entidad de contexto.</param>
        void Delete<T>(T pItem) where T : class;

        /// <summary>
        /// Procesa las propiedades de una entidad para identificar el tipo de operación que se debe realizar en las
        /// referencias simples o múltiples. Para el caso de referencias múltiples (listas o colecciones), además se
        /// verifica si el item ha sido eliminado comparando con las relacciones existentes en la base de datos.
        /// </summary>
        /// <typeparam name="T">Tipo de entidad de contexto.</typeparam>
        /// <param name="pItem">Entidad de contexto.</param>
        void ProcessEntity<T>(T pItem) where T : class;

        /// <summary>
        /// Aplica las modificaciones hechas en <paramref name="pCurrent"/> sobre <paramref name="pOriginal"/>.
        /// </summary>
        /// <typeparam name="T">Tipo de entidad de contexto.</typeparam>
        /// <param name="pOriginal">Entidad de contexto original.</param>
        /// <param name="pCurrent">Entidad de contexto actual.</param>
        void ApplyCurrentValues<T>(T pOriginal, T pCurrent) where T : class;

        /// <summary>
        /// Devuelve el valor de clave primaria principal.
        /// </summary>
        /// <typeparam name="TEntity">Tipo de entidad de contexto.</typeparam>
        /// <param name="pItem">Entidad de contexto.</param>
        /// <returns>Valor de la clave primaria.</returns>
        /// <remarks>No se consideran claves compuestas.</remarks>
        object GetKeyValue<TEntity>(TEntity pItem) where TEntity : class;

    }

}
