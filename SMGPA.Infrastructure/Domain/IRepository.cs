﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Infrastructure.Domain
{
    /// <summary>
    /// Interfaz de repositorio base.
    /// </summary>
    /// <typeparam name="T">Tipo de entidad de dominio.</typeparam>
    public interface IRepository<T> where T : Entity
    {
        /// <summary>
        /// Devuelve el contexto de trabajo.
        /// </summary>
        IUnitOfWork Context { get; }

        /// <summary>
        /// Agrega un nuevo item al repositorio.
        /// </summary>
        /// <param name="pItem">Entidad de dominio.</param>
        void Add(T pItem);

        /// <summary>
        /// Modifica un item.
        /// </summary>
        /// <param name="pItem">Entidad de dominio.</param>
        void Modify(T pItem);

        /// <summary>
        /// Remueve un item.
        /// </summary>
        /// <param name="pID">ID de la entidad de dominio.</param>
        void Remove(object pID);

        /// <summary>
        /// Inserta o actualiza una entidad a partir de su ID.
        /// </summary>
        /// <param name="pItem">Entidad de dominio.</param>
        void InsertOrUpdate(T pItem);

        /// <summary>
        /// Inserta o actualiza una lista de entidad a partir de su ID.
        /// </summary>
        /// <param name="pItems">Entidad de dominio.</param>
        void ListInsertOrUpdate(List<T> pItems);

        /// <summary>
        /// Verifica si un item existe en base de datos y lo actualiza. En caso contrario, lo agrega como nuevo.
        /// NOTA: Para el caso de listas, elimina o anula aquellos items que no se encuentren en la lista pero que
        /// existan en la base de datos.
        /// </summary>
        /// <param name="pItem">Entidad de dominio.</param>
        void ProcessEntity(T pItem);

        /// <summary>
        /// Sobrescribe las propiedades de un item persistido.
        /// </summary>
        /// <param name="pPersisted">Entidad de dominio persistida.</param>
        /// <param name="pCurrent">Entidad de dominio actual.</param>
        void Merge(T pPersisted, T pCurrent);

        /// <summary>
        /// Realiza el seguimiento del item dentro del repositorio (adjunta el item al repositorio).
        /// </summary>
        /// <param name="pItem">Entidad de dominio.</param>
        void Track(T pItem);

        /// <summary>
        /// Devuelve un item a partir de su ID.
        /// </summary>
        /// <param name="pID">ID de la entidad de dominio.</param>
        /// <returns>Entidad de dominio encontrada.</returns>
        T Get(object pID);

        /// <summary>
        /// Devuelve todos los items del repositorio.
        /// </summary>
        /// <returns>Lista de entidades de dominio.</returns>
        IEnumerable<T> GetAll();
    }
}
