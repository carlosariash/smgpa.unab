﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Infrastructure.Domain
{
    public interface IUnitOfWork : IDisposable
    {

        /// <summary>
        /// Acepta todos los cambios realizados en la unidad de trabajo.
        /// </summary>
        void Commit();

        /// <summary>
        /// Cancela y reestablece los cambios realizados en la unidad de trabajo.
        /// </summary>
        void Rollback();

        /// <summary>
        /// Acepta todos los cambios y actualiza los datos de la unidad de trabajo.
        /// </summary>
        void CommitAndRefresh();

        /// <summary>
        /// Ejecuta una consulta T-SQL utilizando la entidad de contexto.
        /// </summary>
        /// <typeparam name="TEntity">Tipo de entidad de contexto.</typeparam>
        /// <param name="pSqlQuery">Consulta T-SQL.</param>
        /// <param name="pParameters">Parámetros de la consulta.</param>
        IEnumerable<TEntity> ExecuteQuery<TEntity>(string pSqlQuery, params object[] pParameters);

        /// <summary>
        /// Ejecuta un comando T-SQL.
        /// </summary>
        /// <param name="pSqlCommand">Comando T-SQL.</param>
        /// <param name="pParameters">Parámetros del comando.</param>
        /// <returns>Cantidad de filas afectadas.</returns>
        int ExecuteCommand(string pSqlCommand, params object[] pParameters);


        /// <summary>
        /// carga diferida para cargar objetos relacionados
        /// </summary>
        /// <param name="Items">booleano para activar o desactivar carga de objetos</param>
        void LazyLoadingEnabled(bool Items);

    }
}
