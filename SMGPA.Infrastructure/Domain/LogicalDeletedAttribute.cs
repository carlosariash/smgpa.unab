﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Infrastructure.Domain
{
    /// <summary>
    /// Atributo que se utiliza para identificar la propiedad utilizada para borrado lógico.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class LogicalDeletedAttribute : Attribute
    {

    }
}
