﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Reflection;
using System.Collections;

namespace SMGPA.Infrastructure.Domain
{
    public class MainUnitOfWork : IQueryableUnitOfWork
    {
        private bool m_isDisposed;

        private DbContext m_context;

        public MainUnitOfWork(DbContext context) : base()
        {
            m_context = context;
            m_context.Database.CommandTimeout = 360;
            //  m_context.Configuration.LazyLoadingEnabled = true;
        }

        #region IQueryableUnitOfWork Members

        public bool Exists<TEntity>(TEntity pItem)
          where TEntity : class
        {
            return m_context.Set<TEntity>().Local.Any(e => e == pItem);


        }




        /// <summary>
        /// LLama a un Procedimiento que resuelve un modelo 
        /// </summary>
        /// <param name="query">Nombre de Procedimiento de almacenado.</param>
        //When you expect a model back
        public IEnumerable<T> ExecWithStoreProcedure<T>(string query)
        {
            return m_context.Database.SqlQuery<T>(query).ToList();
        }




        /// <summary>
        /// LLama a un Procedimiento sin esperar un modelo (asincrónico)
        /// </summary>
        /// <param name="query">Nombre de Procedimiento de almacenado.</param>
        /// <param name="parameters">Parametros de SP.</param>
        public async Task ExecuteWithStoreProcedureAsync(string query, params object[] parameters)
        {
            await m_context.Database.ExecuteSqlCommandAsync(query, parameters);
        }

        /// <summary>
        /// LLama a un Procedimiento sin esperar un modelo
        /// </summary>
        /// <param name="query">Nombre de Procedimiento de almacenado.</param>
        /// <param name="parameters">Parametros de SP.</param>
        public void ExecuteWithStoreProcedure(string query, params object[] parameters)
        {
            m_context.Database.ExecuteSqlCommand(query, parameters);
        }


        public DbSet<TEntity> CreateSet<TEntity>()
            where TEntity : class
        {
            return m_context.Set<TEntity>();
        }

        public void Attach<TEntity>(TEntity pItem)
          where TEntity : class
        {
            // Adjunta el item y cambia su estado como sin modificar.
            m_context.Entry<TEntity>(pItem).State = EntityState.Unchanged;
        }

        public void SetModified<TEntity>(TEntity pItem)
          where TEntity : class
        {
            m_context.Entry<TEntity>(pItem).State = EntityState.Modified;
        }

        /// <summary>
        /// Elimina (anula) la entidad de contexto.
        /// </summary>
        /// <typeparam name="T">Tipo de entidad de contexto.</typeparam>
        /// <param name="pItem">Entidad de contexto.</param>
        public void Delete<T>(T pItem) where T : class
        {
            PropertyInfo wAnuladoProp = GetPropertyAnnulled(pItem);

            if (wAnuladoProp != null)
            {
                // Borrado lógico
                wAnuladoProp.SetValue(pItem, true);
                m_context.Entry(pItem).State = EntityState.Modified;
            }
            else
            {
                // Borrado físico
                m_context.Set(pItem.GetType()).Remove(pItem);
            }
        }

        public void ProcessEntity<TEntity>(TEntity pItem)
          where TEntity : class
        {
            // NOTA: se debe desactiva la creación de entidades proxy para poder navegar los datos de las entidades mediante reflection.
            try
            {
                ((IObjectContextAdapter)m_context).ObjectContext.ContextOptions.ProxyCreationEnabled = false;

                InsertOrUpdateEntity(pItem);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ((IObjectContextAdapter)m_context).ObjectContext.ContextOptions.ProxyCreationEnabled = true;
            }
        }

        public void ApplyCurrentValues<TEntity>(TEntity pOriginal, TEntity pCurrent)
          where TEntity : class
        {
            // Si no está adjuntado, adjunta el objeto original y lo modifica a sus valores actuales.
            if (m_context.Entry<TEntity>(pOriginal).State != EntityState.Deleted)
                m_context.Entry<TEntity>(pOriginal).CurrentValues.SetValues(pCurrent);
        }

        /// <summary>
        /// Acepta todos los cambios realizados en el contexto.
        /// </summary>
        public void Commit()
        {
            m_context.SaveChanges();
        }

        /// <summary>
        /// Cancela y reestablece los cambios realizados en el contexto.
        /// </summary>
        public void Rollback()
        {
            // Asigna todas las entidades en el tracker como sin modificar.
            m_context.ChangeTracker
              .Entries()
              .ToList()
              .ForEach(entry => entry.State = EntityState.Unchanged);
        }

        /// <summary>
        /// Acepta todos los cambios y actualiza los datos del contexto.
        /// </summary>
        public void CommitAndRefresh()
        {
            bool wSaveFailed = false;

            do
            {
                try
                {
                    m_context.SaveChanges();

                    wSaveFailed = false;
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    wSaveFailed = true;

                    ex.Entries.ToList().ForEach(entry =>
                    {
                        entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                    });

                }
                catch (Exception ex)
                {
                    throw ex;
                }

            } while (wSaveFailed);
        }

        /// <summary>
        /// Ejecuta una consulta T-SQL utilizando la entidad de contexto.
        /// </summary>
        /// <typeparam name="TEntity">Tipo de entidad de contexto.</typeparam>
        /// <param name="pSqlQuery">Consulta T-SQL.</param>
        /// <param name="pParameters">Parámetros de la consulta.</param>
        /// <returns>Lista de entidades de contexto de tipo <typeparamref name="TEntity"/>.</returns>
        public IEnumerable<TEntity> ExecuteQuery<TEntity>(string pSqlQuery, params object[] pParameters)
        {
            return m_context.Database.SqlQuery<TEntity>(pSqlQuery, pParameters);
        }

        /// <summary>
        /// Ejecuta un comando T-SQL.
        /// </summary>
        /// <param name="pSqlCommand">Comando T-SQL.</param>
        /// <param name="pParameters">Parámetros del comando.</param>
        /// <returns>Cantidad de filas afectadas.</returns>
        public int ExecuteCommand(string pSqlCommand, params object[] pParameters)
        {
            return m_context.Database.ExecuteSqlCommand(pSqlCommand, pParameters);
        }

        public void LazyLoadingEnabled(bool Items)
        {
            m_context.Configuration.LazyLoadingEnabled = Items;
        }



        /// <summary>
        /// Devuelve el valor de clave primaria principal.
        /// </summary>
        /// <typeparam name="TEntity">Tipo de entidad de contexto.</typeparam>
        /// <param name="pItem">Entidad de contexto.</param>
        /// <returns>Valor de la clave primaria.</returns>
        /// <remarks>No se consideran claves compuestas.</remarks>
        public object GetKeyValue<TEntity>(TEntity pItem) where TEntity : class
        {
            return pItem.GetType().GetProperty(GetKeyNames<TEntity>()[0]).GetValue(pItem);
        }

        #endregion

        #region Private Members

        /// <summary>
        /// Devuelve la propiedad de anulación de una entidad de contexto.
        /// </summary>
        /// <param name="wItem">Entidad de contexto.</param>
        /// <returns>Propiedad anulado de entidad de contexto.</returns>
        private PropertyInfo GetPropertyAnnulled(object wItem)
        {
            foreach (PropertyInfo wPInfo in this.GetType().GetProperties())
            {
                if (wPInfo.CustomAttributes.Any(p => p.AttributeType == typeof(LogicalDeletedAttribute)))
                {
                    return wPInfo;
                }
            }

            return null;
        }

        /// <summary>
        /// Devuelve el valor de clave primaria principal.
        /// </summary>
        /// <param name="wPropItem">Tipo de entidad de contexto.</param>
        /// <returns>Valor de la clave primaria.</returns>
        /// <remarks>Este método se realiza mediante reflection por no poder reconocer el tipo de entidad de contexto.</remarks>
        private object GeyKeyValue(object wPropItem)
        {
            return this.GetType().GetMethod("GetKeyValue").MakeGenericMethod(wPropItem.GetType()).Invoke(this, new object[] { wPropItem });
        }

        /// <summary>
        /// Devuelve la lista de claves primarias en formato cadena de una entidad de contexto.
        /// </summary>
        /// <typeparam name="T">Tipo de entidad de contexto.</typeparam>
        /// <returns>Lista de claves primarias.</returns>
        /// <remarks>
        /// IMPORTANTE! todas las tablas deben tener clave principal única y de tipo identidad para que esto funcione.
        /// </remarks>
        private string[] GetKeyNames<T>()
          where T : class
        {
            var objectSet = ((IObjectContextAdapter)m_context).ObjectContext.CreateObjectSet<T>();

            string[] keyNames = objectSet.EntitySet.ElementType.KeyMembers
                                                               .Select(k => k.Name)
                                                               .ToArray();
            return keyNames;
        }

        /// <summary>
        /// Modifica o agrega una entidad de contexto según su ID.
        /// </summary>
        /// <param name="pItem">Entidad de contexto.</param>
        public void InsertOrUpdateEntity<TEntity>(TEntity pItem)
          where TEntity : class
        {
            if (pItem != null)
            {
                // Llama de forma recursiva.
                this.GetType().GetMethod("ProcessEntityRelationships").MakeGenericMethod(pItem.GetType()).Invoke(this, new object[] { pItem });

                // Actualiza o inserta según el ID del item.
                var wPropItemKeyValue = GeyKeyValue(pItem);

                // TODO: reconocer el tipo principal! hardcode con int
                if ((int)wPropItemKeyValue != 0)
                {
                    var wPersistedPropItem = m_context.Set(pItem.GetType()).Find(wPropItemKeyValue);

                    ApplyCurrentValues(wPersistedPropItem, pItem);
                }
                else
                {
                    m_context.Set(pItem.GetType()).Add(pItem);
                }
            }
        }

        /// <summary>
        /// Procesa las propiedades de una entidad para identificar el tipo de operación que se debe realizar en las
        /// referencias simples o múltiples. Para el caso de referencias múltiples (listas o colecciones), además se
        /// verifica si el item ha sido eliminado comparando con las relacciones existentes en la base de datos.
        /// <typeparam name="TEntity">Tipo de entidad de contexto.</typeparam>
        /// <param name="pItem">Entidad de contexto.</param>
        public void ProcessEntityRelationships<TEntity>(TEntity pItem) where TEntity : class
        {
            TEntity wPersistedItem = null;
            bool isNewItem = false;

            // Obtiene la clave principal
            var wKeyValue = GetKeyValue<TEntity>(pItem);

            // TODO: hardcode int
            if ((int)wKeyValue == 0)
                isNewItem = true;
            else
                wPersistedItem = m_context.Set(pItem.GetType()).Find(wKeyValue) as TEntity;

            // Actualiza estados de referencias.

            List<PropertyInfo> wClassesPropertiesInfo = pItem.GetType().GetProperties().Where(p =>
              p.PropertyType.IsClass && p.PropertyType != typeof(string) && !p.PropertyType.IsArray &&
              p.PropertyType != typeof(DateTime) && p.PropertyType != typeof(Guid)).ToList();

            var getMethod = pItem.GetType().GetMethod("Get");

            // NOTA: se comenta la inserción o actualización de relaciones simples debido a que se produce un
            // stack overflow cuando en referencias múltiples se vuelve a utilizar la entidad principal. Para
            // solucionar esto, se sugiere que toda relación se realice a partir del ID de entidad y no mediante
            // la entidad misma (descomentado este código se realizaría también por el objeto de la entidad)
            /*
            foreach (PropertyInfo wPropInfo in wClassesPropertiesInfo)
            {
              var item = pItem.GetType().GetProperty(wPropInfo.Name).GetValue(pItem);

              if (item != null) InsertOrUpdateEntity(item);
            }
            */

            // NOTA: al insertar/modificar una relación, si la mista posee el objeto, se duplican datos.
            // NOTA: se comenta debido a que ahora trabaja bien si el ID de la entidad está bien asignado a la
            // propiedad referencial.
            // Como vamos a trabajar solo con los IDs, vamos a eliminar el objeto de la relación.
            /*
            foreach (PropertyInfo wPropInfo in wClassesPropertiesInfo)
            {
              pItem.GetType().GetProperty(wPropInfo.Name).SetValue(pItem, null);
            }
            */

            // Actualiza estados de referencias múltiples (colecciones).
            List<PropertyInfo> wCollectionPropertiesInfo = pItem.GetType().GetProperties().Where(p =>
              p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(ICollection<>)).ToList();

            foreach (PropertyInfo wCollectionPropInfo in wCollectionPropertiesInfo)
            {
                var wCollectionProp = (IEnumerable)pItem.GetType().GetProperty(wCollectionPropInfo.Name).GetValue(pItem);

                // TODO: validar que pasa en caso que una lista venga como null y existan datos en la base de datos.
                // Se debería anular/eliminar todos... esto se hace?

                // Anula las entidades que se hayan eliminado en el cliente.
                if (!isNewItem) DeleteChildEntitiesFromCollection<TEntity>(pItem, wPersistedItem, wCollectionPropInfo);

                // Identifica si el item posee un ID para realizar un alta o una modificación.
                foreach (var wPropItem in wCollectionProp)
                {
                    InsertOrUpdateEntity(wPropItem);
                }
            }

        }

        /// <summary>
        /// Elimina (anula) las entidades hijas de una propiedad de tipo colección de una entidad de contexto.
        /// </summary>
        /// <typeparam name="TEntity">Tipo de entidad de contexto.</typeparam>
        /// <param name="pItem">Entidad de contexto modificada.</param>
        /// <param name="wPersistedItem">Entidad de contexto actualmente persistida.</param>
        /// <param name="wCollectionPropInfo">Propiedad de tipo ICollection<> de la entidad de contexto.</param>
        private void DeleteChildEntitiesFromCollection<TEntity>(TEntity pItem, TEntity wPersistedItem, PropertyInfo wCollectionPropInfo) where TEntity : class
        {
            List<object> itemsToDelete = new List<object>();

            var wCollectionProp = (IEnumerable)pItem.GetType().GetProperty(wCollectionPropInfo.Name).GetValue(pItem);

            var wPersistedCollectionProp = (IEnumerable)wPersistedItem.GetType().GetProperty(wCollectionPropInfo.Name).GetValue(wPersistedItem);

            // Recorre los elementos que existen en base de datos y fueron eliminados.
            foreach (var wPersistedPropItem in wPersistedCollectionProp)
            {
                bool wIsDeleted = true;

                // Obtiene los valores del item persistido (sin proxy).
                var wPersistedPropItemValue = m_context.Entry(wPersistedPropItem).CurrentValues.ToObject();

                foreach (var wPropItem in wCollectionProp)
                {
                    // TODO: reconocer el tipo principal! hardcode con int
                    // NOTA: tener en cuenta que si encuentra una coincidencia es que el item NO se eliminó.
                    if ((int)GeyKeyValue(wPropItem) == (int)GeyKeyValue(wPersistedPropItemValue)) wIsDeleted = false;
                }

                if (wIsDeleted) itemsToDelete.Add(wPersistedPropItem);
            }

            foreach (object item in itemsToDelete)
                Delete(item);
        }

        #endregion

        #region IDisposable Members

        protected virtual void Dispose(bool pDisposing)
        {
            if (!this.m_isDisposed)
            {
                if (pDisposing)
                {
                    m_context.Dispose();
                }
            }
            this.m_isDisposed = true;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        public IList<T> ExecWithStoreProcedure<T>(string query, params object[] parameters)
        {
            return m_context.Database.SqlQuery<T>(query, parameters).ToList();
        }

        #endregion
    }
}
