﻿using SMGPA.Infrastructure.Adapting;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Infrastructure.Domain
{
    public abstract class Repository<TEntity, TEntityModel> : IQueryableRepository<TEntity, TEntityModel>
      where TEntityModel : Entity, new()
      where TEntity : class, new()
    {

        #region Members

        IQueryableUnitOfWork m_unitOfWork;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="pUnitOfWork">Unidad de trabajo.</param>
        public Repository(IQueryableUnitOfWork pUnitOfWork)
        {
            if (pUnitOfWork == (IUnitOfWork)null) throw new ArgumentNullException();

            m_unitOfWork = pUnitOfWork;
        }

        #endregion

        #region IRepository Members

        /// <summary>
        /// Devuelve el contexto de trabajo.
        /// </summary>
        public IUnitOfWork Context
        {
            get { return m_unitOfWork; }
        }

        /// <summary>
        /// Agrega un nuevo item.
        /// </summary>
        /// <param name="pItem">Entidad de dominio.</param>
        public virtual void Add(TEntityModel pItem)
        {
            if (pItem != null)
            {
                TEntity dbItem = GetSet().Add(pItem.ConvertTo<TEntity>());

                m_unitOfWork.CommitAndRefresh();

                pItem.SetEntityKey(dbItem.ConvertTo<TEntityModel>().GetEntityKey());
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Modifica un item.
        /// </summary>
        /// <param name="pItem">Entidad de dominio.</param>
        public virtual void Modify(TEntityModel pItem)
        {
            if (pItem != null)
            {
                m_unitOfWork.ApplyCurrentValues(
                  GetSet().Find(pItem.GetEntityKey()),
                  pItem.ConvertTo<TEntity>()
                );

                m_unitOfWork.CommitAndRefresh();
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Remueve un item.
        /// </summary>
        /// <param name="pID">ID de la entidad de dominio.</param>
        public virtual void Remove(object pID)
        {
            // TODO: obtener si existe aguna propiedad de anulado y cambiar su estado en vez de remover.
            if (pID != null)
            {
                TEntity wEntity = GetSet().Find(pID);

                m_unitOfWork.Delete<TEntity>(wEntity);
                m_unitOfWork.CommitAndRefresh();
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Inserta o actualiza una entidad a partir de su ID.
        /// </summary>
        /// <param name="pItem">Entidad de dominio.</param>
        public virtual void InsertOrUpdate(TEntityModel pItem)
        {
            var entityKey = pItem.GetEntityKey();
            if ((int)entityKey == 0)
                Add(pItem);
            else
                Modify(pItem);
        }

        /// <summary>
        /// Inserta o actualiza una entidad a partir de su ID.
        /// </summary>
        /// <param name="pItem">Entidad de dominio.</param>
        public virtual void ListInsertOrUpdate(List<TEntityModel> pItems)
        {
            foreach (TEntityModel pItem in pItems)
            {
                var entityKey = pItem.GetEntityKey();


                if ((int)entityKey == 0)
                {
                    TEntity dbItem = GetSet().Add(pItem.ConvertTo<TEntity>());
                    m_unitOfWork.CommitAndRefresh();
                    pItem.SetEntityKey(dbItem.ConvertTo<TEntityModel>().GetEntityKey());

                }
                else
                {

                    m_unitOfWork.ApplyCurrentValues(GetSet().Find(pItem.GetEntityKey()), pItem.ConvertTo<TEntity>());
                    m_unitOfWork.CommitAndRefresh();
                }

            }
        }














        /// <summary>
        /// Verifica si un item existe en base de datos y lo actualiza. En caso contrario, lo agrega como nuevo.
        /// NOTA: Para el caso de listas, elimina o anula aquellos items que no se encuentren en la lista pero que
        /// existan en la base de datos.
        /// </summary>
        /// <param name="pItem">Entidad de dominio.</param>
        public void ProcessEntity(TEntityModel pItem)
        {
            if (pItem != null)
                m_unitOfWork.ProcessEntity(pItem.ConvertTo<TEntity>());
            else
                throw new NullReferenceException();
        }

        /// <summary>
        /// Conecta una entidad persistida con una en memoria fuera del contexto.
        /// </summary>
        /// <param name="pPersisted">Entidad persistida.</param>
        /// <param name="pCurrent">Entidad fuera del contexto.</param>
        public virtual void Merge(TEntityModel pPersisted, TEntityModel pCurrent)
        {
            object dbId = pPersisted.GetEntityKey();

            // FIX ID de entidad current
            pCurrent.SetEntityKey(dbId);

            var dbPersisted = pPersisted.ConvertTo<TEntity>();
            var dbCurrent = pCurrent.ConvertTo<TEntity>();

            m_unitOfWork.ApplyCurrentValues(
              GetSet().Find(dbId),
              dbCurrent
            );

            m_unitOfWork.CommitAndRefresh();
        }

        /// <summary>
        /// Agrega al contexto una entidad externa.
        /// </summary>
        /// <param name="pItem">Entidad de dominio.</param>
        public virtual void Track(TEntityModel pItem)
        {
            if (pItem != null)
                m_unitOfWork.Attach<TEntity>(pItem.ConvertTo<TEntity>());
            else
                throw new NullReferenceException();
        }

        /// <summary>
        /// Devuelve un item a partir de su ID.
        /// </summary>
        /// <param name="pID">ID de la entidad de dominio.</param>
        /// <returns>Entidad de dominio encontrada.</returns>
        public virtual TEntityModel Get(object pID)
        {
            return TypeAdapterFactory.Current.Adapt<TEntity, TEntityModel>(GetSet().Find(pID));
        }

        /// <summary>
        /// Devuelve la lista completa de entidades.
        /// </summary>
        /// <returns>Lista completa de entidades de dominio.</returns>
        public virtual IEnumerable<TEntityModel> GetAll()
        {
            return ConvertToModelList(GetSet());
        }

        /// <summary>
        /// Devuelve la lista completa de entidades.
        /// </summary>
        /// <returns>Lista completa de entidades de dominio.</returns>
        public virtual IEnumerable<T> GetAllGeneric<T>() where T : class, new()
        {
            return ConvertToModelListGeneric<T>(GetSet());
        }

        /// <summary>
        /// Convierte una entidade de contexto en una entidade de dominio.
        /// </summary>
        /// <param name="pEntity">Entidade de contexto.</param>
        /// <returns>Entidade de dominio.</returns>
        public virtual TEntityModel ConvertToModel(TEntity pEntity)
        {
            return TypeAdapterFactory.Current.Adapt<TEntity, TEntityModel>(pEntity);
        }

        /// <summary>
        /// Convierte una lista de entidades de contexto en una lista de entidades de dominio.
        /// </summary>
        /// <param name="pList">Lista de entidades de contexto.</param>
        /// <returns>Lista de entidades de dominio.</returns>
        public virtual IEnumerable<TEntityModel> ConvertToModelList(IEnumerable<TEntity> pList)
        {
            return pList.Select(e => TypeAdapterFactory.Current.Adapt<TEntity, TEntityModel>(e));
        }

        /// <summary>
        /// Convierte una lista de entidades de contexto en una lista de DTO Generico.
        /// </summary>
        /// <param name="pList">Lista de entidades de contexto.</param>
        /// <returns>lista de DTO Generico</returns>
        protected IEnumerable<T> ConvertToModelListGeneric<T>(IEnumerable<TEntity> pList) where T : class, new()
        {
            return pList.Select(e => TypeAdapterFactory.Current.Adapt<TEntity, T>(e));
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Libera los recursos del repositorio.
        /// </summary>
        public void Dispose()
        {
            if (m_unitOfWork != null)
                m_unitOfWork.Dispose();

            GC.SuppressFinalize(this);
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Devuelve la colección de entidades dentro del contexto.
        /// </summary>
        /// <returns>Colección de entidades dentro del contexto.</returns>
        protected IDbSet<TEntity> GetSet()
        {
            return m_unitOfWork.CreateSet<TEntity>();
        }

        /// <summary>
        /// Devuelve la colección de entidades dentro del contexto.
        /// </summary>
        /// <typeparam name="T">Tipo de entidad de contexto.</typeparam>
        /// <returns>Colección de entidades dentro del contexto.</returns>
        protected IDbSet<T> GetSet<T>()
          where T : class
        {
            return m_unitOfWork.CreateSet<T>();
        }

        #endregion

    }
}
