﻿using AutoMapper;
using SMGP.EntFk;
using SMGPA.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Mappers
{
    public class ProfileConfiguration : Profile
    {

        public override string ProfileName
        {
            get
            {
                return this.GetType().Name;
            }
        }
        protected override void Configure()
        {

            Mapper.CreateMap<SMGP.EntFk.Activity, Domain.Entities.Activitys>()
                .ReverseMap();

            Mapper.CreateMap<SMGP.EntFk.Career, Domain.Entities.Careers>()
                .ReverseMap();


            Mapper.CreateMap<SMGP.EntFk.Document, Domain.Entities.Documents>()
                .ReverseMap();

            Mapper.CreateMap<SMGP.EntFk.Faculty, Domain.Entities.Facultys>()
    .ReverseMap();


            Mapper.CreateMap<SMGP.EntFk.Group, Domain.Entities.Groups>()
    .ReverseMap();


            Mapper.CreateMap<SMGP.EntFk.Member, Domain.Entities.Members>()
.ReverseMap();

            //            Mapper.CreateMap<SMGP.EntFk.Notificacion, Domain.Entities.no>()
            //.ReverseMap();

            Mapper.CreateMap<SMGP.EntFk.Observation, Domain.Entities.Observations>()
.ReverseMap();

            Mapper.CreateMap<SMGP.EntFk.Parameter, Domain.Entities.Parameters>()
.ReverseMap();


            //            Mapper.CreateMap<SMGP.EntFk.Permission, Domain.Entities.per>()
            //.ReverseMap();

            Mapper.CreateMap<SMGP.EntFk.PermissionRole, Domain.Entities.PermissionRoles>()
.ReverseMap();

            Mapper.CreateMap<SMGP.EntFk.Role, Domain.Entities.Roles>()
.ReverseMap();

            Mapper.CreateMap<SMGP.EntFk.Task, Domain.Entities.TaskActivity>()
.ReverseMap();

            Mapper.CreateMap<SMGP.EntFk.TypeParameter, Domain.Entities.TypeParameters>()
.ReverseMap();


            Mapper.CreateMap<SMGP.EntFk.University, Domain.Entities.Universitys>()
.ReverseMap();

            Mapper.CreateMap<SMGP.EntFk.User, Domain.Entities.Users>()
.ReverseMap();


            Mapper.CreateMap<SMGP.EntFk.Venue, Domain.Entities.Venues>()
                .ReverseMap();
        }
    }
}
