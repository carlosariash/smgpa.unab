﻿using SMGP.EntFk;
using SMGPA.Domain.Entities;
using SMGPA.Domain.Seedwork;
using SMGPA.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMGPA.Repositories.Repositories
{
    public class VenuesRepository : Repository<SMGP.EntFk.Venue, Domain.Entities.Venues>, IVenuesRepository
    {
        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="pUnitOfWork">Unidad de trabajo.</param>
        public VenuesRepository(MainUnitOfWork pUnitOfWork)
      : base(pUnitOfWork)
        {

        }

        #endregion
    }
}
