
export class ProcessesView {
    ProcessesID: number;
    ProcessesName: string;
    StartDate: Date;
    EndDate: Date;
    TypeProcesses: string;
    State: string;
    Carrer: string;
    University: string;
    Faculty: string;
    ResponsbilityName: string;
    Percentage: Number;
}

export class Processes {
    ProcessesID: number;
    ProcessesName: string;
    StartDate: Date;
    EndDate: Date;
    TypeProcessesID?: number;
    CarrererID?: number;
    UniversityID?: number;
    ResponsbilityID?: number;
    VenuesID?: number;
    FacultyID?: number;
    Personal: string[];

}

