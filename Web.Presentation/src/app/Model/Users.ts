export class PersonalView {
    UserID: number;
    Run: string;
    Name: string;
    LastName: string;
    CompleteName: string;
    TypePersonal?: number;
}
