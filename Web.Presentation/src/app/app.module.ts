import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule} from '@angular/router';
import {ModalModule} from 'ngx-bootstrap';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';

import {ROUTES} from './app.routes';
import { AppComponent } from './app.component';

// App views
import {DashboardsModule} from './views/dashboards/dashboards.module';
import {SettingsModule} from './views/settings/settings.module';
import {AppviewsModule} from './views/appviews/appviews.module';
import {ProcessesModule} from './views/processes/processes.module'

// App modules/components
import {LayoutsModule} from './components/common/layouts/layouts.module';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {ProcessesService, WebServices, ParametersService, FacultyService, CarrerService, VenuesService, UserService} from './services';




@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    SweetAlert2Module.forRoot(),
    ModalModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule,
    DashboardsModule,
    LayoutsModule,
    AppviewsModule,
    SettingsModule,
    ProcessesModule,
    AngularFontAwesomeModule,
    RouterModule.forRoot(ROUTES)
  ],
  // tslint:disable-next-line:max-line-length
  providers: [ProcessesService, WebServices, ParametersService, FacultyService, CarrerService, VenuesService,
    UserService,  {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
