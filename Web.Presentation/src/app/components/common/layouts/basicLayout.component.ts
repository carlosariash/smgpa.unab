import { Component } from '@angular/core';
import { detectBody } from '../../../app.helpers';

declare var jQuery: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'basic',
  templateUrl: 'basicLayout.template.html',
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    '(window:resize)': 'onResize()'
  }
})
export class BasicLayoutComponent {

  // tslint:disable-next-line:use-life-cycle-interface
  public ngOnInit(): any {
    detectBody();
  }

  public onResize() {
    detectBody();
  }

}
