import { WebServices } from '../WebServices.Services';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CarrersView } from 'app/Model/Carrers';
@Injectable()
export class CarrerService  {
    constructor(private http: HttpClient, private  _WebServices: WebServices) { }
    getAllCarrersView() {
        return this.http.get<CarrersView[]>(this._WebServices.ApiGetAllCarrersView);
    }
}
