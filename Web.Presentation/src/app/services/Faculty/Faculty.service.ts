import { WebServices } from '../WebServices.Services';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FacultyView } from 'app/Model';
@Injectable()
export class FacultyService  {
    constructor(private http: HttpClient, private  _WebServices: WebServices) { }
    getAllFacultyView() {
        return this.http.get<FacultyView[]>(this._WebServices.ApiGetAllFacultyView);
    }
}
