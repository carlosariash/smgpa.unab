import { WebServices } from '../WebServices.Services';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ParametersView } from 'app/Model';

@Injectable()
export class ParametersService  {
    constructor(private http: HttpClient, private  _WebServices: WebServices) { }

    getAllParametersByType(Id: number) {
        return this.http.get<ParametersView[]>(this._WebServices.ApiGetParameterByType + Id);
    }
}
