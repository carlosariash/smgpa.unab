import { WebServices } from '../WebServices.Services';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PersonalView } from 'app/Model';
import { EnumTypePersonal } from 'app/Model/Enums/EnumsDomain';
@Injectable()
export class UserService  {
    constructor(private http: HttpClient, private  _WebServices: WebServices) { }
    getAllPersonalView() {
        return this.http.get<PersonalView[]>(this._WebServices.ApiGetAllUsers);
    }
    getAllPersonalType(Type: EnumTypePersonal) {
        return this.http.get<PersonalView[]>(this._WebServices.ApiGetAllUsersType + Type);
    }
}
