import { WebServices } from '../WebServices.Services';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { VenuesView } from 'app/Model';
@Injectable()
export class VenuesService  {
    constructor(private http: HttpClient, private  _WebServices: WebServices) { }
    getAllVnuesView() {
        return this.http.get<VenuesView[]>(this._WebServices.ApiGetAllVenuesView);
    }
}
