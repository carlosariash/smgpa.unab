import { Injectable } from '@angular/core';
@Injectable()
export class WebServices {
    public Server = 'http://localhost:5685/api/';
    public ApiGetAllProcesses = this.Server + 'Processes/GetAllProcesses';
    public ApiGetParameterByType = this.Server + 'Parameter/GetAllByTypeParameter?IdType=';
    public ApiGetAllFacultyView = this.Server + 'Faculty/GetAllFacultyView';
    public ApiGetAllCarrersView = this.Server + 'Carrers/GetAllCarrersView';
    public ApiGetAllVenuesView = this.Server + 'Venues/GetAllVenusView';
    public ApiGetAllUsers = this.Server + 'Users/GetAllUsers';
    public ApiGetAllUsersType = this.Server + 'Users/GetAllPersonalType?idType=';
    public ApiCreateProcesses = this.Server + 'Processes/CreateProcesse';
 }
