// tslint:disable-next-line:eofline
export * from './processes/processes.service';
export * from './WebServices.Services';
export * from './Parameters/parameters.service';
export * from './Faculty/Faculty.service';
export * from './Carrers/Carrers.service';
export * from './Venues/Venues.service';
export * from './Users/User.service';
