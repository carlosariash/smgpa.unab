import { WebServices } from '../WebServices.Services';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ProcessesView, Processes} from '../../Model';
import { RequestOptions } from '@angular/http';
@Injectable()
export class ProcessesService  {
    constructor(private http: HttpClient, private  _WebServices: WebServices) { }

    getAllProcesses() {
        return this.http.get<ProcessesView[]>(this._WebServices.ApiGetAllProcesses);
    }
    CreateProcesse(Item: Processes) {
        return this.http.post(this._WebServices.ApiCreateProcesses, Item );
    }
}
