import { Component, OnDestroy, OnInit, } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'starter',
  templateUrl: 'starter.template.html',
  styleUrls: ['starter.style.scss']
})
export class StarterViewComponent implements OnDestroy, OnInit  {

public nav: any;
public backGround: any;

public constructor() {
  this.nav = document.querySelector('nav.navbar');
 // this.backGround = document.getElementById('page-wrapper');
}

public ngOnInit(): any {
  this.nav.className += ' white-bg';
//  this.backGround.className += ' bg-img';

}


public ngOnDestroy(): any {
  this.nav.classList.remove('white-bg');
 // this.backGround.classList.remove('bg-img');
}

}
