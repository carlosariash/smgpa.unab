// tslint:disable-next-line:eofline
import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { first } from 'rxjs/operators';
import { Observable } from 'rxjs';
import * as Enumerable from 'linq-es2015';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {Router} from '@angular/router';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'DetailsProcesse',
    templateUrl: 'Details-Processe.template.html',
    styleUrls: ['Details-Processe.style.scss']
  })
  export class DetailsProcesseComponent implements OnDestroy, OnInit {
    public nav: any;
    public constructor() {}
    ngOnInit(): void {
        this.nav.className += ' white-bg';
    }
    ngOnDestroy(): void {
        this.nav.classList.remove('white-bg');
    }
  }
