import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { first } from 'rxjs/operators';
import { Observable } from 'rxjs';
 import * as Enumerable from 'linq-es2015';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {Router} from '@angular/router';
import { ProcessesView, Processes, ParametersView, FacultyView , VenuesView, CarrersView, PersonalView,
  EnumTypePersonal} from '../../Model';
import { ProcessesService, ParametersService, FacultyService, CarrerService, VenuesService, UserService } from '../../services';
import { DxTreeViewComponent } from 'devextreme-angular';


@Component({
    // tslint:disable-next-line:component-selector
    selector: 'processe',
    templateUrl: 'processe.template.html',
    styleUrls: ['processe.style.scss']
  })
  export class ProcesseComponent implements OnDestroy, OnInit {
    public nav: any;
    modalRef: BsModalRef;
    @ViewChild(DxTreeViewComponent) treeView;
    ViewProcesses: ProcessesView[] = [];
    TypeProcessesView: ParametersView[] = [];
    FacultyView: FacultyView[] = [];
    CarrerView: CarrersView[] = [];
    VenueView: VenuesView[] = [];
    Processes: Processes = new Processes();
    PersonalAssinated: PersonalView[] = [];
    PersonalView: PersonalView[] = [];
    ProcessesId: string;
    treeBoxValue: string[];
    startValue: Date = new Date();
    endValue: Date = new Date();
public constructor(private modalService: BsModalService, private router: Router, private _ProcessesService: ProcessesService,
  private _ParametersService: ParametersService, private _FacultyService: FacultyService, private _CarrerService: CarrerService,
  private _VenuesService: VenuesService, private _UserService: UserService) {
  this.nav = document.querySelector('nav.navbar');
}
public ngOnInit(): any {
  this.nav.className += ' white-bg';
  this.GetAllProcesses();
}

GetAllProcesses() {
  this._ProcessesService.getAllProcesses().pipe(first()).subscribe(processes => {
    this.ViewProcesses = processes;
});

}
CancelNewProcesses() {
  this.Processes = new Processes();
  this.modalRef.hide();
}

GetHeader() {

  this._ParametersService.getAllParametersByType(2).pipe(first()).subscribe(Typeprocesses => {
    this.TypeProcessesView = Typeprocesses; });

  this._FacultyService.getAllFacultyView().pipe(first()).subscribe(Faculty => {
      this.FacultyView = Faculty; });

  this._CarrerService.getAllCarrersView().pipe(first()).subscribe(Carrers => {
        this.CarrerView = Carrers; });

  this._VenuesService.getAllVnuesView().pipe(first()).subscribe(Venue => {
          this.VenueView = Venue; });

  this._UserService.getAllPersonalView().pipe(first()).subscribe(Personal => {
    this.PersonalAssinated = Enumerable.asEnumerable(Personal).Where(x => x.TypePersonal === EnumTypePersonal.Functionary).ToArray();
    this.PersonalView = Personal;
  });



}

CreateProcesses(params) {
  const  result = params.validationGroup.validate();
  if (result.isValid) {
    this.Processes.Personal = this.treeBoxValue;
    this.Processes.StartDate = this.startValue;
    this.Processes.EndDate = this.endValue;
    this._ProcessesService.CreateProcesse(this.Processes).subscribe(
      data => {
        notify('Proceso Creado con Exito', 'success', 700);
       // this.GetAllAssets();
        return true;
      },
      error => {
        notify('Problemas al Crear el Proceso', 'error', 700);
      return Observable.throw(error);
      }
  );
   }
}

OpenAddCreateProcesses(template: TemplateRef<any>) {
  this.GetHeader();
  this.modalRef = this.modalService.show(template, {class: 'modal-lg'});
}


public ngOnDestroy(): any {
  this.nav.classList.remove('white-bg');
}

syncTreeViewSelection(e) {
  // tslint:disable-next-line:prefer-const
  let component = (e && e.component) || (this.treeView && this.treeView.instance);

  // tslint:disable-next-line:curly
  if (!component) return;

  if (!this.treeBoxValue) {
      component.unselectAll();
  }

  if (this.treeBoxValue) {
      this.treeBoxValue.forEach((function (value) {
          component.selectItem(value);
      }).bind(this));
  }
}

treeView_itemSelectionChanged(e) {
  const nodes = e.component.getNodes();
  this.treeBoxValue = this.getSelectedItemsKeys(nodes);
}

getSelectedItemsKeys(items) {
  let result = [],
      // tslint:disable-next-line:prefer-const
      that = this;
  items.forEach(function(item) {
      if (item.selected) {
          result.push(item.key);
      }
      if (item.items.length) {
          result = result.concat(that.getSelectedItemsKeys(item.items));
      }
  });
  return result;
}
}
