import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {ProcesseComponent} from './processe.component';
// tslint:disable-next-line:max-line-length
import { DxDataGridModule, DxTextBoxModule, DxSelectBoxModule, DxValidatorModule, DxDateBoxModule, DxTreeViewModule, DxDropDownBoxModule,
DxListModule, DxValidationGroupModule, DxButtonModule, DxValidationSummaryModule} from 'devextreme-angular';
import { FormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { DxiValidationRuleModule } from 'devextreme-angular/ui/nested/validation-rule-dxi';
import { DetailsProcesseComponent } from './Details-Processe.component';
@NgModule({
    declarations: [ProcesseComponent, DetailsProcesseComponent
    ],
    imports: [
      BrowserModule,
      DxDataGridModule,
      FormsModule,
      SweetAlert2Module,
      DxTextBoxModule,
      DxSelectBoxModule,
      DxValidatorModule,
      DxDateBoxModule,
      DxTreeViewModule,
      DxDropDownBoxModule,
      DxListModule,
      DxValidationGroupModule,
      DxButtonModule,
      DxValidationSummaryModule,
      DxiValidationRuleModule
    ],
    exports: [ProcesseComponent, DetailsProcesseComponent
    ],
    entryComponents: []
  })
  export class ProcessesModule {
}
