import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {ParameterComponent} from './parameter.component';


@NgModule({
    declarations: [ParameterComponent
    ],
    imports: [
      BrowserModule
    ],
    exports: [ParameterComponent
    ],
  })
  export class SettingsModule {
}
